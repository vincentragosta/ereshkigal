<?php

namespace Ereshkigal\Block\ModalCard;

use Ishtar\View\ComponentView;

/**
 * Class ModalCardView
 * @package Ereshkigal\Block\ModalCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $picks
 */
final class ModalCardView extends ComponentView
{
    protected $name = 'modal-card';
    protected static $default_properties = [
        'picks' => ''
    ];

    public function __construct($picks)
    {
        parent::__construct(compact('picks'));
    }
}

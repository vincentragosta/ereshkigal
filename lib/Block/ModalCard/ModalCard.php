<?php

namespace Ereshkigal\Block\ModalCard;

use Ereshkigal\Block\Block;
use Ishtar\View\View;

/**
 * Class ModalCard
 * @package Ereshkigal\Block\ModalCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $name
 * @property View $View
 */
final class ModalCard extends Block
{
    protected $name = 'modal-card';

    public function render($attributes, $content, $block)
    {
        $View = $this->getView();
        return new $View($attributes['picks']);
    }
}

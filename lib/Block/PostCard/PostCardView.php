<?php

namespace Ereshkigal\Block\PostCard;

use Ishtar\View\ComponentView;

/**
 * Class PostCardView
 * @package Ereshkigal\Block\PostCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $picks
 */
final class PostCardView extends ComponentView
{
    protected $name = 'post-card';
    protected static $default_properties = [
        'picks' => ''
    ];

    public function __construct($picks)
    {
        parent::__construct(compact('picks'));
    }
}

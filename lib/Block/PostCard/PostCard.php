<?php

namespace Ereshkigal\Block\PostCard;

use Ereshkigal\Block\Block;
use Ishtar\View\View;

/**
 * Class PostCard
 * @package Ereshkigal\Block\PostCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $name
 * @property View $View
 */
final class PostCard extends Block
{
    protected $name = 'post-card';

    public function render($attributes, $content, $block)
    {
        $View = $this->getView();
        return new $View($attributes['picks']);
    }
}

<?php
/**
 * Expected:
 * @var string $picks
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Ishtar\Utility\ComponentUtility;

?>

<div <?= ComponentUtility::attributes('post-card', $class_modifiers, $element_attributes); ?>>
    <?php var_dump($picks); ?>
</div>

<?php

namespace Ereshkigal\Block;

/**
 * Class Block
 * @package Ereshkigal\Block
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
abstract class Block
{
    protected $name;
    protected $manifest = '';
    protected $View = null;
    protected $manifest_path = WP_PLUGIN_DIR . '/ereshkigal';

    public function register()
    {
        $block_config = $this->getManifestContent();
        $block_config['render_callback'] = [$this, 'render'];
        register_block_type($block_config['name'], $block_config);
    }

    public function getManifest()
    {
        if (empty($this->manifest)) {
            $this->manifest = sprintf('%s/manifest/%s.json', $this->manifest_path, $this->name);
        }
        return $this->manifest;
    }

    public function getManifestContent(): array
    {
        if (!file_exists($this->getManifest())) {
            return [];
        }
        return json_decode(file_get_contents($this->getManifest(), true), TRUE);
    }

    public function getView()
    {
        if (empty($this->View)) {
            $this->View = sprintf('%s\%s', $this->getChildClassNamespace(), $this->getClassName());
        }
        return $this->View;
    }

    public function getChildClassNamespace()
    {
        return substr(get_called_class(), 0, strrpos(get_called_class(), "\\"));
    }

    public function getClassName()
    {
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $this->name))) . 'View';
    }

    abstract function render($attributes, $content, $block);
}

<?php
/*
Plugin Name: Ereshkigal
Description: An anime-themed hybrid framework built on top of WordPress (Gutenberg). This plugin is used to make gutenberg blocks in PHP and react.
Version: 1.0
License: GPL-2.0+
*/

use Ereshkigal\Block;
use Gilgamesh\Service\RegisterExtensionService;

defined('ABSPATH') || exit;
if (! defined('USE_COMPOSER_AUTOLOADER') || ! USE_COMPOSER_AUTOLOADER) {
    require __DIR__ . '/vendor/autoload.php';
}

/**
 * Class Ereshkigal
 * @author  Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class Ereshkigal
{
    const EXTENSIONS = [
        Block\PostCard\PostCard::class,
        Block\ModalCard\ModalCard::class
    ];

    public function __construct()
    {
        add_action('gilgamesh/init', function() {
            (new RegisterExtensionService(static::EXTENSIONS))->run();
        });
    }
}

new Ereshkigal();
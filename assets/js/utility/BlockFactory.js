const { registerBlockType } = wp.blocks;

/**
 * class BlockFactory
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class BlockFactory {
    /**
     * Creates a new Gutenberg Block with hidden attributes to support previews.
     *
     * @param {object} manifest The manifest data from file.
     * @param {object} config The edit & save handler object.
     * @returns {boolean}
     */
    static create(manifest, config) {
        // manifest.example = {
        //     attributes: {
        //         _preview: true,
        //     },
        // };
        //
        // manifest.attributes._preview = {
        //     type: 'string',
        //     default: false,
        // };

        if (!config.save) {
            config.save = () => {
                return null;
            };
        }

        const blockType = Object.assign(manifest, config);
        return registerBlockType(manifest.name, blockType);
    }
}

export default BlockFactory;

import manifest from '../../../manifest/post-card';
import PostPicker from '../components/PostPicker';
import BlockFactory from '../utility/BlockFactory';

const { __ } = wp.i18n;
const {PanelBody} = wp.components;
const {InspectorControls, useBlockProps} = wp.blockEditor;
const ServerSideRender = wp.serverSideRender || wp.components.ServerSideRender;

const PostCard = BlockFactory.create(manifest, {
    edit: ((props) => {
        const {setAttributes, attributes} = props;
        const blockProps = useBlockProps();

        let {picks} = attributes;
        picks = picks === '' || typeof picks === 'undefined' ? [] : picks.split(',');

        return (
            <div {...blockProps}>
                <ServerSideRender block='ereshkigal/post-card' attributes={attributes} />
                <InspectorControls>
                    <PanelBody title={__('Selected Posts', 'ereshkigal')} key={'posts'}>
                        <PostPicker
                            isMulti={false}
                            initialSelection={picks}
                            postTypes={['post']}
                            onChange={(picks) => {
                                setAttributes({picks});
                            }}
                        />
                    </PanelBody>
                </InspectorControls>
            </div>
        )
    })
});

export default PostCard;

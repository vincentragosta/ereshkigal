import ResourcePicker from '../resources/ResourcePicker';
import PostResource from '../resources/PostResource';

/**
 * PostPicker is the react component for building React Select based
 * post pickers. It uses a corresponding PostResource object as the source of the data.
 */
class PostPicker extends ResourcePicker {
    /**
     * Initializes the Resource Picker
     *
     */
    constructor() {
        super(...arguments); // eslint-disable-line
        this.getResource = this.getResource.bind(this);
    }

    /**
     * Lazy initializes the post resource object.
     *
     * @param {object} props The component props.
     * @returns {PostResource}
     */
    getResource(props) {
        if (this.resource) {
            return this.resource;
        }
        const postTypes = props.postTypes || ['post', 'page'];
        this.resource = new PostResource(postTypes);
        return this.resource;
    }
}

export default PostPicker;

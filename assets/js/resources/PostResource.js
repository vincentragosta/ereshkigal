import Resource from './Resource';

const {wp, lodash} = window;
const {apiFetch} = wp;
const {addQueryArgs} = wp.url;

/**
 * PostResource is a Resource object that fetches post resources via a custom
 * search endpoint. Search has 2 modes,
 *
 * - searching by user typed query
 * - searching by post ids
 */
class PostResource extends Resource {
    /**
     * Initializes parent resource
     *
     * @param {Array} postTypes The post types for the resource.
     */
    constructor(postTypes) {
        super(...arguments); // eslint-disable-line
        this.postTypes = postTypes || ['post', 'page'];
    }

    /**
     * Fetches the Post objects for the specified term ids
     *
     * @param {Array} ids The specified post ids
     * @returns {Promise}
     */
    fetchByIDs(ids) {
        if (!ids || ids.length === 0) {
            return [];
        }
        const opts = {
            post_ids: ids.join(','), // eslint-disable-line camelcase
        };
        return this.fetch(opts);
    }

    /**
     * Fetches the Post terms matching the specified search phrase.
     *
     * @param {string} query The search phrase
     * @returns {Promise}
     */
    fetchByQuery(query) {
        const opts = {
            per_page: 10, // eslint-disable-line camelcase
        };
        if (query) {
            opts.search = query;
        }
        opts.post_types = this.getPostTypes().join(','); // eslint-disable-line camelcase
        return this.fetch(opts);
    }

    /**
     * Post label field
     *
     * @returns {string}
     */
    getLabelField() {
        return 'title';
    }

    /**
     * Post id field
     *
     * @returns {string}
     */
    getValueField() {
        return 'id';
    }

    /**
     * Fetches the Posts over REST API
     *
     * @param {object} opts Query options
     * @returns {Promise}
     */
    fetch(opts) {
        const endpoint = this.getEndpoint();
        // Note: This merge is left to right
        opts = lodash.merge({}, this.getQueryOpts(), opts); // eslint-disable-line
        const queryParams = {
            path: addQueryArgs(endpoint, opts),
        };
        return apiFetch(queryParams);
    }

    /**
     * Returns the list of post types that can be searched by this endpoint.
     *
     * @returns {string}
     */
    getPostTypes() {
        return this.postTypes;
    }

    /**
     * Returns the REST endpoint for this Resource
     *
     * @returns {string}
     */
    getEndpoint() {
        return `/wp/v2/search`;
    }

    /**
     * Returns the default query opts to pass to the server.
     *
     * @returns {Array}
     */
    getQueryOpts() {
        return {
            _context: 'edit',
        };
    }
}

export default PostResource;

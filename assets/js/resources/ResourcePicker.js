import AsyncSelect from '../../../../../themes/vincent-ragosta/node_modules/react-select/async';

const {wp} = window;
const {Component} = wp.element;
const {Spinner} = wp.components;

/**
 * ResourcePicker is the base react component for building React Select based
 * pickers. It uses a corresponding Resource object as the source of the data.
 */
class ResourcePicker extends Component {
    /**
     * Initializes the Resource Picker
     */
    constructor() {
        super(...arguments); // eslint-disable-line
        this.state = {
            loaded: false,
        };
        this.onChange = this.onChange.bind(this);
        this.getResource = this.getResource.bind(this);
        this.idMapper = this.idMapper.bind(this);
    }

    /**
     * Fetch Selected Terms from the Resource
     */
    componentDidMount() {
        const self = this;
        const {initialSelection} = this.props;
        const resource = this.getResource(this.props);
        if (!resource.getSelection()) {
            resource.loadSelection(initialSelection).then((results) => {
                self.setState({loaded: true});
            });
        } else {
            self.setState({loaded: true});
        }
    }

    /**
     * Renders the ResourceSelect component
     *
     * @returns {object}
     */
    render() {
        if (!this.state.loaded) {
            return <Spinner/>;
        }
        const {isMulti} = this.props;
        const resource = this.getResource(this.props);
        return (
            <AsyncSelect
                menuPortalTarget={document.body}
                styles={{menuPortal: (base) => ({...base, zIndex: 99999})}}
                isMulti={isMulti}
                isClearable={true}
                defaultValue={resource.getSelection()}
                loadOptions={resource.getFinder()}
                defaultOptions={true}
                onChange={this.onChange}
                getOptionLabel={resource.getOptionLabel()}
                getOptionValue={resource.getOptionValue()}
            />
        );
    }

    /**
     * Updates the data sources connected to this Resource Select
     *
     * @param {object} selection The new selection
     * @param {object} opts Optional opts
     */
    onChange(selection, {action}) {
        const {onChange} = this.props;
        if (!onChange) {
            return;
        }
        switch (action) {
            case 'select-option':
            case 'remove-value':
                onChange(this.serializeSelection(selection));
                break;
            case 'clear':
                onChange(this.serializeSelection(null));
                break;
            default:
                break;
        }
    }

    /**
     * Saves the selection in the Control attributes
     *
     * @param {object} selection The new selection
     * @returns {string}
     */
    serializeSelection(selection) {
        const {setAttributes} = this.props;
        const multiple = this.props.isMulti;
        const resource = this.getResource(this.props);
        let picks;
        if (multiple) {
            const values = selection
                ? lodash.map(selection, this.props.idMapper || this.idMapper)
                : [];
            resource.selectedIDs = values;
            resource.selection = selection || [];
            picks = lodash.join(values, ',');
        } else {
            const value = selection ? selection.id : '';
            resource.selectedIDs = selection ? [selection.id] : [];
            resource.selection = selection ? [selection] : [];
            picks = `${value}`;
        }
        return picks;
    }

    /**
     * Lazy initializes the resource object.
     *
     * @param {object} props The component props.
     * @returns {object}
     */
    getResource(props) {
        /* abstract */
        return null;
    }

    /**
     * Extracts item id from item.
     *
     * @param {object} item The item to map
     * @returns {number}
     */
    idMapper(item) {
        return item.id || false;
    }
}

export default ResourcePicker;

// try doing ~@vincentragosta for this
import debounce from '../../../../../themes/vincent-ragosta/node_modules/debounce-promise';

const {wp} = window;
const {decodeEntities} = wp.htmlEntities;

/**
 * Resource is an Async Data Source for use with the ResourceSelect
 * component. This is an Abstract class. Methods marked must be extended
 * by subclasses.
 */
class Resource {
    /**
     * Initializes the resource with the default selection
     *
     * @param {Array} selectedIDs The array of selected ids
     */
    constructor(selectedIDs) {
        this.selectedIDs = selectedIDs || [];
        this.selection = null;
        const interval = this.getDebounceInterval();
        this.searchByQuery = debounce(this.fetchByQuery.bind(this), interval);
        this.searchByIDs = debounce(this.fetchByIDs.bind(this), interval);
    }

    /**
     * Returns the debounce interval.
     *
     * @returns {number}
     */
    getDebounceInterval() {
        return 300;
    }

    /**
     * Fetches results by searching on queried phrase.
     *
     * @param {string} query The query phrase
     * @returns {Promise}
     */
    fetchByQuery(query) {
        /* abstract */
        return null;
    }

    /**
     * Fetches the objects for the object ids
     *
     * @param {Array} ids List of queried object ids
     * @returns {Promise}
     */
    fetchByIDs(ids) {
        /* abstract */
        return null;
    }

    /**
     * Loads up the initial selection of ids
     *
     * @param {Array} ids Optional ids to seed immediately
     * @returns {Promise}
     */
    loadSelection(ids) {
        if (ids) {
            this.selectedIDs = ids;
        }
        if (this.selectedIDs.length === 0) {
            return new Promise((resolve) => {
                resolve([]);
            });
        }
        const self = this;
        return this.searchByIDs(this.selectedIDs).then((results) => {
            self.selection = results;
            return results;
        });
    }

    /**
     * Returns the currently selected objects
     *
     * @returns {Array}
     */
    getSelection() {
        return this.selection;
    }

    /**
     * Returns the Finder object for this resource. This object is bound
     * to the <AsyncSelect>
     *
     * @returns {Function}
     */
    getFinder() {
        return this.searchByQuery.bind(this);
    }

    /**
     * Default object label field
     *
     * @returns {string}
     */
    getLabelField() {
        return 'label';
    }

    /**
     * Default object value field
     *
     * @returns {string}
     */
    getValueField() {
        return 'value';
    }

    /**
     * Label function to lookup label from an option
     *
     * @returns {Function}
     */
    getOptionLabel() {
        const labelField = this.getLabelField();
        return (option) => {
            return decodeEntities(option[labelField]);
        };
    }

    /**
     * Value function to lookup value from an option
     *
     * @returns {Function}
     */
    getOptionValue() {
        const valueField = this.getValueField();
        return (option) => {
            return option[valueField];
        };
    }
}

export default Resource;
